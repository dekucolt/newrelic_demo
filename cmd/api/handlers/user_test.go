package handlers_test

import (
	"net/http"
	"sync"
	"testing"
)

func TestRespond(t *testing.T) {
	wg := &sync.WaitGroup{}
	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go func(wg *sync.WaitGroup) {
			defer wg.Done()
			http.Get("https://localhost:8080/users")
		}(wg)
	}

	wg.Wait()
}
