package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/newrelic/go-agent/v3/newrelic"
	"math/rand"
	"net/http"
	"time"
)

type User struct {
	Username  string
	Email     string
	Firstname string
	Lastname  string
}

var (
	errorMessage = map[string]string{
		"message": "Internal Server Error",
	}
	badRequest = map[string]string{
		"message": "Bad Request",
	}
)

func getUsers() []User {
	users := []User{
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
		User{
			Username:  "aybarrel",
			Email:     "aybarrel@gmail.com",
			Firstname: "Aybar",
			Lastname:  "Zholamanov",
		},
	}
	return users
}

func Respond(w http.ResponseWriter, statusCode int, data any) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	res, _ := json.Marshal(data)
	w.Write(res)
}

func getAll(w http.ResponseWriter, r *http.Request) {
	rand.Seed(time.Now().UnixNano())
	num := rand.Intn(2)
	if num == 1 {
		Respond(w, http.StatusOK, getUsers())
		return
	}
	if num == 2 {
		Respond(w, http.StatusBadRequest, badRequest)
		return
	}
}

func API(relic *newrelic.Application) *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc(newrelic.WrapHandleFunc(relic, "/users", getAll)).Methods(http.MethodGet)

	return router
}
