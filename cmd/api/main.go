package main

import (
	"demosrv/cmd/api/handlers"
	"github.com/newrelic/go-agent/v3/newrelic"
	"log"
	"net/http"
	"os"
)

func main() {
	log := log.New(os.Stdout, "AYBAR'S DEMO SERVICE ", log.Flags())
	log.Println("Starting...")
	app, err := newrelic.NewApplication(
		newrelic.ConfigAppName("demosrv"),
		newrelic.ConfigLicense("eu01xx18eab1fc38864b0ff6ad152adba85eNRAL"),
		newrelic.ConfigAppLogForwardingEnabled(true),
	)

	if err != nil {
		log.Fatalf("fatal err : %v", err)
	}

	router := handlers.API(app)

	server := http.Server{
		Addr:    "localhost:8080",
		Handler: router,
	}

	log.Println("Server successfully started on http://localhost:8080")

	if err := server.ListenAndServe(); err != nil {
		log.Fatalf("fatal server error : %v", err)
	}
}
